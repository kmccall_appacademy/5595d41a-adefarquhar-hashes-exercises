# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_hash = {}
  str.split.each { |word| word_hash[word] = word.length }
  word_hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  sorted = hash.sort_by { |key, val| val }
  sorted.last[0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each { |item, val| older[item] = val }
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letter_freq = Hash.new(0)
  word.chars.each { |letter| letter_freq[letter] += 1 }
  letter_freq
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  nums = Hash.new(0)
  arr.each { |num| nums[num] += 1 }
  nums.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  counts = {even: 0, odd: 0}
  numbers.each do |num|
    num.even? ? counts[:even] += 1 : counts[:odd] += 1
  end

  counts
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowel_freq = Hash.new(0)
  string.chars.each { |vwl| vowel_freq[vwl] += 1 }

  max_freq = vowel_freq.values.max
  most_common = vowel_freq.reject do |vwl, val|
    val < max_freq
  end

  most_common.min[0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  jul_dec_bday = students.select { |name, month| month > 6 }.keys

  combos = []
  jul_dec_bday.each_with_index do |name, idx|
    next if name == jul_dec_bday.last
    jul_dec_bday[idx+1..-1].each do |name1|
      combos << [name, name1]
    end
  end

  combos
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  species_count = Hash.new(0)
  specimens.each { |spec| species_count[spec] += 1 }

  num_species = species_count.length
  pop_size_min = species_count.values.min
  pop_size_max = species_count.values.max
  biodiv_index = num_species**2 * pop_size_min/pop_size_max
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  norm_ltr_freqs = character_count(normal_sign)
  vand_ltr_freqs = character_count(vandalized_sign)

  vand_ltr_freqs.each do |ltr, freq|
    return false unless freq <= norm_ltr_freqs[ltr]
  end

  true
end

def character_count(str)
  str = str.downcase.delete("^a-z")
  letters = str.chars
  count = Hash.new(0)
  letters.each { |ltr| count[ltr] += 1 }
  count
end
